(ns sgr-server.config
  (:require [sgr-svm.model :refer [read-model]]
            [sgr-svm.train :refer [train-model]]
            [sgr-svm.model    :refer [write-model]]
            [sgr-svm.dataset  :refer [write-dataset]]))

(def user-dir (System/getProperty "user.dir"))
(def private-resources (str user-dir "/resources/private/svm/"))

(def audio-config {:male   {:source "/home/ivan/Work/clojure/src/sgr/sgr-audio/source/male_audio/"
                            :target "/home/ivan/Work/clojure/src/sgr/sgr-audio/target/male_audio/"}
                   :female {:source "/home/ivan/Work/clojure/src/sgr/sgr-audio/source/female_audio/"
                            :target "/home/ivan/Work/clojure/src/sgr/sgr-audio/target/female_audio/"}
                   :source "/home/ivan/Work/clojure/src/sgr/sgr-audio/source/"
                   :target "/home/ivan/Work/clojure/src/sgr/sgr-audio/target/"})

(def svm-config {:path-model   (str private-resources   "modelUSE_RBF_C-SVC")
                 :path-dataset (str private-resources "datasetUSE_RBF_C-SVC")
                 :model        (fn
                                 ([] (read-model (str private-resources "modelUSE_RBF_C-SVC")))
                                 ([dataset]
                                  (let [model (train-model dataset)]
                                    (write-dataset dataset (str private-resources "datasetUSE_RBF_C-SVC"))
                                    (write-model model     (str private-resources   "modelUSE_RBF_C-SVC"))
                                    model)))})

(defn get-audio-config
  [gender]
  (case gender
    "male"    (:male   audio-config)
    "female"  (:female audio-config)
    "unknown" {:source (:source audio-config)
               :target (:target audio-config)}))

