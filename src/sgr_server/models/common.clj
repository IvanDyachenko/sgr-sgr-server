(ns sgr-server.models.common
  (:require [sgr-server.config :as cfg]))

(defn uuid [] (str (java.util.UUID/randomUUID)))

(defn save-tempfile
  "Save file and return path."
  ([path tempfile]
   (do (clojure.java.io/copy tempfile (clojure.java.io/file path)) path))
  ([filename path tempfile]
   (save-tempfile (str path filename) tempfile)))


(defn get-source-target-path
  {:doc
   "result:
   {:sourcePath sourcePath :targetPath targetPath}"}
  [tempfile & {:keys [gender] :or {gender "unknown"}}]
  (let [id (uuid)
        name (str "original-" gender "-" id ".wav")
        sourcePath (save-tempfile name (:source (cfg/get-audio-config gender)) tempfile)
        targetName (str "converted-" gender "-" id ".wav")
        targetPath (str (:target (cfg/get-audio-config gender)) targetName)]
    {:sourcePath sourcePath :targetPath targetPath}))

(defn get-path
  [file]
  (.getPath file))

(defn percent-predict [predict]
  (let [p (+ 1.001 predict)]
    (/ (* p 100) 2)))
