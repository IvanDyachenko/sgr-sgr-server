$.notify.addStyle("train", {
  html:
  "<div>" +
    "<div class='notify-train'>" +
      "<button class='btn red' style='cursor: pointer;'>Male</button>" +
      "<button class='btn blue' style='cursor: pointer';>Female</button>" +
    "</div>" +
  "</div>",
  classes: {
    base: {
      "font-weight": "bold",
      "padding": "8px 15px 8px 14px",
      "text-shadow": "0 1px 0 rgba(255, 255, 255, 0.5)",
      "background-color": "#fcf8e3",
      "border": "1px solid #fbeed5",
      "border-radius": "4px",
      "white-space": "nowrap",
      "background-repeat": "no-repeat",
      "background-position": "3px 7px",
      "color": "#3A87AD",
      "background-color": "#D9EDF7",
      "border-color": "#BCE8F1"
    }
  }
});

$(document).on('click', '.notifyjs-train-base .red', function() {
   postRecord('male');
  $(this).trigger('notify-hide');
});
$(document).on('click', '.notifyjs-train-base .blue', function() {
   postRecord('female');
  $(this).trigger('notify-hide');
});

function postRecord(gender)
{
  recorder.exportWAV(function(blob) { var fd = new FormData();
                                      fd.append('gender', gender);
                                      fd.append('record', blob);
                                      $.ajax({
                                          url: '/train/record',
                                          type: 'post',
                                          dataType: 'json',
                                          contentType: false,
                                          processData: false,
                                          data: fd,
                                          success: function(result, status, xhr) {
                                             updateCharts(result);
                                             console.log('ajax post filestorage success: '+ result +' '+ status +' '+ xhr);
                                           },
                                           error: function(xhr, status, error) {
                                             console.log('ajax  post filestorage error: '+ xhr +' '+ status +' '+ error);
                                           }});});
}

function toggleTrain(r)
{
  if (!recorder) {
    startAudio(function() {toggleRecording(r); toggleSinewave(r);});
  }
  else {
    toggleSinewave(r);
     if (toggleRecording(r))
     {
       $(".nav-notify").notify({}, {style: 'train',
                                    arrowShow: true,
                                    className: 'info',
                                    autoHide: true,
                                    autoHideDelay: 4000,
                                    clickToHide: false});
     }
  }
}




