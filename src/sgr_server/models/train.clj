(ns sgr-server.models.train
  (:require [sgr-server.config        :as     cfg]
            [sgr-server.models.common :refer :all]

            [sgr-db.utils     :as db]

            [sgr-sound.converter :refer [sound->convert->mfcc->average]]

            [sgr-svm.predict  :as svmp]
            [sgr-svm.dataset  :refer [mfcc->svm-mfcc data->svm-data get-dataset-from-db]]))


(defn train-record
  ([gender tempfile]
   (let [source-target (get-source-target-path tempfile :gender (str gender))
         sourceFile (clojure.java.io/file (:sourcePath source-target))
         targetFile (clojure.java.io/file (:targetPath source-target))]
     (train-record gender sourceFile targetFile)))
  ([gender sourceFile targetFile]
   (try
     (let [convert (sound->convert->mfcc->average sourceFile targetFile)
           mfcc    (:mfcc convert)
           user    {:name "unknown" :gender gender :genderSVM "unknown" :SVM? true}
           records (list {:path (get-path sourceFile) :converted? false :mfcc false}
                         {:path (get-path targetFile) :converted? true  :mfcc  mfcc})
           result   (db/create-user<-add-records user records)
           dataset  (get-dataset-from-db);;(concat (get-dataset-from-db)
                                         ;;(data->svm-data (list {:mfcc mfcc :gender gender})))
           predict  (svmp/predict ((:model cfg/svm-config) dataset) (mfcc->svm-mfcc mfcc))]
       {:predict (int (percent-predict predict)) :mfcc (map #(int %) mfcc)})
     (catch Exception e
       (do
         (println (str "Exception SGR-server.models.train-record: " (.getMessage e)))
         {:predict false :mfcc false})))))

