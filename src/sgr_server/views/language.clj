(ns sgr-server.views.language)

(def locale {:en "?locale=en_EN"
             :ru "?locale=ru_RU"})

(def layout {:title {:en "Speaker recognition using ANFIS"
                     :ru "Speaker recognition using ANFIS"}
             :logo {:en " Speaker recognition"
                    :ru " Speaker recognition"}
             :recognition {:en "recognition"
                           :ru "распознавание"}
             :train {:en "train"
                     :ru "обучение"}
             :statistics {:en "statistics"
                          :ru "статистика"}
             :author {:en "Dyachenko Ivan"
                      :ru "Dyachenko Ivan"}
             :year {:en "2014, ",
                    :ru "2014, "}
             :label {:ru "chose language: "
                     :en "выбрать язык: "}
             :language {:ru "english"
                        :en "русский"}
             :chose {:en "?locale=ru_RU"
                     :ru "?locale=en_EN"}})

(def home {:h1 {:en "Speaker recognition"
                :ru "Speaker recognition"}
           :h2 {:en "Speaker gender recognition via MFCCs and SVMs"
                :ru "Распознавание пола диктора с использованием MFCC и SVM"}
           :btn-red {:en "Recognize"
                     :ru "Распознать"}
           :btn-blue {:en " Train "
                      :ru "Обучить"}
           :train {:en "System learning"
                   :ru "Обучение ситемы"}
           :about-train {:en "Add your audio-records for further system learning."
                         :ru "Добавляйте аудио-записи Вашиx голосов для дальнейшего обучения системы."}
           :statistics {:en "Statistics"
                        :ru "Статистика"}
           :about-statistics {:ru "Знакомьтесь с актуальной статистикой."
                              :en "Watch up to date statistics."}
           :mfcc {:en "MFCC"
                  :ru "МЧКК"}
           :recognize {:en "Recognition"
                       :ru "Распознавание"}
           :about-recognize {:en "Speaker gender recognition via mel-frequency cepstral coefficients and support vector machines."
                             :ru "Распознавание диктора осуществляется с помощью мел-частот кепстральныx коэффициентов и метода опорныx векторов."}})

(def train {:h1 {:en "System learning"
                 :ru "Обучение системы"}
            :intro {:en "Everything you need to know to add your record"
                    :ru "Добавляйте аудио-записи Вашиx голосов для дальнейшего обучения системы."}
            :record-article {:en "Recording"
                             :ru "Запись голоса"}
            :record-text-first {:en "First you need to record your voice."
                                :ru "Прежде всего, необходимо записать Вaш голос. Для начала и завершения записи нажмите на "}
            :record-text-second {:en "First you need to record your voice."
                                 :ru " вначале страницы. Рекомендуемая длительность составляет от 7 до 15 секунд."}
            :gender-article {:ru "Выбор пола"
                             :en "Your gender"}
            :gender-text {:en ""
                          :ru "После завершения записи голоса появится нотификация о выборе пола."}
            :upload-article {:en "Upload on server"
                             :ru "Отправка данных"}
            :upload-text {:en ""
                          :ru "После выбора Ваши данные автоматически отправятся на сервер."}})

(def recognize {:mfcc {:en "MFCC"
                       :ru "МЧКК"}
                :train {:en "Result"
                        :ru "Результат"}
                :statistics {:en "Statistics"
                             :ru "Статистика"}})

