(ns sgr-server.models.statistics
  (:require [sgr-server.config :as cfg]
            [sgr-db.utils      :as  db]))

(defn number-of-<gender>-records []
  (let [m (db/number-of-<gender>-records   "male")
        f (db/number-of-<gender>-records "female")]
    {:male m :female f}))

(defn percent-of-<gender>-records []
  (let [m (db/number-of-<gender>-records   "male")
        f (db/number-of-<gender>-records "female")
        m&f (+ m f)
        mr (int (/ (* m 100) m&f))
        fr (- 100 mr)]
    {:male mr :female fr}))


