(ns sgr-server.views.train
  (:require [sgr-server.views.language :as lng]
            [sgr-server.views.layout :as layout]
            [hiccup.page :refer [html5 include-css include-js]]))

(defn train [& {:keys [language] :or {language ':en}}]
  (layout/common
   (html5
    (include-css "/css/train.css")

    [:div {:class "aspect-ratio"}
     [:canvas {:id "recorder-bar" :width "1920px" :height "1080px"}]
     [:header
      [:input {:id "capture" :onclick "toggleTrain(\"capture\")"
               :class "micr" :name "capture"
               :type "image" :src "/img/micr.svg" :width "15%"}]]]

    [:div {:class "content-main"}
     [:section {:class "features container"}

      [:article
       [:h3 (-> lng/recognize :mfcc language)]
       [:div {:class "canvas-holder hover-highlight"}
        [:canvas {:id "mfcc-line"
                  :width "400" :height "400"
                  :style "width: 400px; height: 400px; cursor: default;"}]]]

      [:article
       [:h3 (-> lng/recognize :train language)]
       [:div {:class "labeled-chart-container"}
        [:div {:class "canvas-holder hover-highlight"}
         [:canvas {:id "train-modular"
                   :width "400" :height "400"
                   :style "width: 400px; height: 400px; cursor: default;"}]]]]

      [:article
       [:h3 (-> lng/recognize :statistics language)]
       [:div {:class "canvas-holder hover-highlight"}
        [:canvas {:id "statistics-line"
                  :width "400" :height "400"
                  :style "width: 400px; height: 400px; cursor: default;"}]]]

      ]]

    (include-js "/js/jquery.min.js")
    (include-js "/js/notify.min.js")
    (include-js "/js/recorderjs/recorderWorker.js")
    (include-js "/js/recorderjs/recorder.js")
    (include-js "/js/Chart.min.js")

    (include-js "/js/sinewave.js")
    (include-js "/js/recognize&train.js")
    (include-js "/js/train.js"))
   :active 'train
   :language language))
