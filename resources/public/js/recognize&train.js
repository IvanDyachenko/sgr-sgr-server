navigator.getUserMedia = (navigator.getUserMedia       ||
                          navigator.webkitGetUserMedia ||
                          navigator.mozGetUserMedia    ||
                          navigator.msGetUserMedia);

var audioCtx = null;
var gainNode = null;
var source   = null;
var analyser = null;

var $id = function(id) {
  return document.getElementById(id);};

var canvas = $id('recorder-bar');
var ctx = canvas.getContext('2d');

var recorder = null;

var sinewave  = null;

var myLineChart = null;

var moduleDoughnut = null;

function saveRecord() {
  recorder.exportWAV(function(blob) { Recorder.forceDownload(blob);});
}

$(document).on('click', '.saverecord', function() {
  saveRecord();
  $(this).trigger('notify-hide');
});

function startAudio(callbackFunction)
{
  try {
    console.log('getUserMedia supported.');
    navigator.getUserMedia({audio: true},
                           function (stream) {successCallback (stream, callbackFunction);},
                           function (err) {errorCallback(err);});
  } catch(e) {
    console.error('Error: getUserMedia() is not supported.', e);
    $(".nav-notify").notify("getUserMedia is not supported in your browser.", {className: 'error',
                                                                               showAnimation: 'show',
                                                                               hideAnimation: 'hide',
                                                                               autoHideDelay: 4000,
                                                                               hideDuration: 400});
  }
}

function errorCallback(err)
{
  console.error('Error: navigator.getUserMedia -> errorCallback.', err);
  message.innerHTML = "<p class='error'>" + err + "</p>";
}

function successCallback(stream, callbackFunction)
{
  try {
    audioCtx = new (window.AudioContext       ||
                    window.webkitAudioContext ||
                    window.mozAudioContext    ||
                    window.oAudioContext      ||
                    window.msAudioContext)();

    /* Creates a GainNode, which can be used to control the overall volume of the audio graph. */
    gainNode = audioCtx.createGain();

    /* Creates a MediaStreamAudioSourceNode associated with a MediaStream representing an audio stream
    which may come from the local computer microphone or other sources. */
    source = audioCtx.createMediaStreamSource(stream);
    source.connect(gainNode);

    /*var oscillator = audioCtx.createOscillator();
    oscillator.channelInterpretation = 'speakers';
    oscillator.connect(gainNode);*/

    /* The createAnalyser() method of the AudioContext interface creates an AnalyserNode,
    which can be used to expose audio time and frequency data and create data visualisations. */
    analyser = audioCtx.createAnalyser();
    //analyser.minDecibels = -90;
    //analyser.maxDecibels = -10;
    //analyser.smoothingTimeConstant = 0.85;
    gainNode.connect(analyser);

    recorder = new Recorder(source);

    /*
    var zeroGain = audioCtx.createGain();
    zeroGain.gain.value = 0.0;
    source.connect(zeroGain);
    zeroGain.connect(audioCtx.destination);
    */

    //$(".nav-notify").notify("Web Audio API is supported.", {className: 'success', autoHideDelay: 2500});

    if (callbackFunction && typeof(callbackFunction) == "function") {
      callbackFunction();
    }
    else {
      console.log('successCallback(stream, callbackFunction): callbackFunction is not satisfied.')
    }

  } catch(e) {
    console.error('Error: Web Audio API is not supported.', e);

    $(".nav-notify").notify("Web Audio API is not supported in your browser.", {className: 'error',
                                                                                showAnimation: 'show',
                                                                                hideAnimation: 'hide',
                                                                                autoHideDelay: 4000,
                                                                                hideDuration: 400});
  }
}

function addOrRemoveClassList(name)
{
  var elements = document.getElementsByName(name);
  var result = false;
  for (var i=0; i<elements.length; i++) {
    var element = elements[i];
    if (element.classList.contains(name)) {
      element.classList.remove(name);
      result = true;
    }
    else {
      element.classList.add(name);
    }
  }
  return result;
}


function toggleRecording(rname) {
  var aor = addOrRemoveClassList(rname);
  var result = false
  if (aor) {
    recorder && recorder.stop();
    console.log('toggleRecording(): Stop recording.');
    result = true;
  }
  else {
    if (recorder) {
        recorder.clear();
        recorder.record();
        console.log('toggleRecording(): Start recording.');
      }
  }
  return result;
}

function toggleSinewave(sname) {
  var ss = document.getElementsByName(sname);
  if (ss.length > 0) {
    var s = ss[0];
    if (s.classList.contains("sinewave")) {
      sinewave.stop();
      //sinewave.static();
      s.classList.remove("sinewave");
      console.log('toggleSinewave(): Stop sinewaving.');
    }
    else {
      if (!sinewave)
        sinewave = new Sinewave(canvas, analyser, "#b9525c");
      sinewave.start();
      s.classList.add("sinewave");
      console.log('toggleSinewave(): Start sinewaving.');
    }
  }
}

function Colour(col, amt) {
  var usePound = false;
  if (col[0] == "#") {
    col = col.slice(1);
    usePound = true;
  }
  var num = parseInt(col,16);
  var r = (num >> 16) + amt;
  if (r > 255) r = 255;
  else if (r < 0) r = 0;
  var b = ((num >> 8) & 0x00FF) + amt;
  if (b > 255) b = 255;
  else if (b < 0) b = 0;
  var g = (num & 0x0000FF) + amt;
  if (g > 255) g = 255;
  else if (g < 0) g = 0;
  return (usePound?"#":"") + (g | (b << 8) | (r << 16)).toString(16);
}

(function(){

  Chart.defaults.global.responsive = true;


	// MFCC
	(function(){
    var canvas = document.getElementById('mfcc-line'),
        ctx = canvas.getContext('2d');

    var data = {
      labels: ["", "", "", "", "","", "", "", "", "","","","", "", "", "", "","", "", "", "", "","",""],
      datasets: [
        {
          label: "About recognize",
          fillColor: "rgba(91,144,191,0.4)",
          strokeColor: "rgba(91,144,191,1)",
          pointColor: "rgba(220,220,220,1)",
          pointStrokeColor: "#fff",
          pointHighlightFill: "rgba(91,144,191,0.8)",
          pointHighlightStroke: "rgba(91,144,191,1)",
          //data: [0,1,2,3,4,9,4,1,0,1,4,9,16,9,4,1,0,1,4,9,16,9,4,1,0,1]
          data: [23, 3, 12, 6, -3, -7, -3, 0, -1, -6, -0, 1, -3, -0, -2, -3, -0, -5, -4, 3, -0, -3, -0, -1, -3, -4]
          //data: [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        }
    ]
    };

    myLineChart = new Chart(ctx).Line(data,
                                      {responsive: true,
                                       animation: true,
                                       onAnimationComplete: function(){
                                         this.options.animation = true;
                                       }});
  })();

  // Train
	(function(){
    var male   = 50;
    var female = 50;
    var canvas = document.getElementById('train-modular'),
        ctx = canvas.getContext('2d'),
        colours = {"Female": "#4c86b9",
                   "Male": "#b9525c"};
    var moduleData = [
      {
        value: male,
        color: colours["Male"],
        highlight: Colour(colours["Male"], 10),
        label: "Male voice on"
      },
      {
        value: female,
        color: colours["Female"],
        highlight: Colour(colours["Female"], 10),
        label: "Female voice on"
      }];

    moduleDoughnut = new Chart(ctx).Doughnut(moduleData,
                                             {responsive: true,
                                              animation: true,

                                              onAnimationComplete: function(){
                                                this.options.animation = true;
                                              }});
  })();

  //Statistics
  (function(){
    var canvas = document.getElementById('statistics-line'),
        ctx = canvas.getContext('2d'),
        lineChartData = [90, 76, 64, 56, 38, 25, 16, 14, 7];

    var data = {
      labels: ["Error", "Male", "Success", "Female"],
      datasets: [
        {
          label: "My First dataset",
          fillColor:   "rgba(151,187,205,0.2)",
          strokeColor: "rgba(151,187,205,1)",
          pointColor:  "rgba(151,187,205,1)",
          pointStrokeColor:   "#fff",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(151,187,205,1)",
          data: [0, 0, 0, 0]
        },
        {
          label: "My Second dataset",
          fillColor: "rgba(220,220,220,0.2)",
          strokeColor: "rgba(220,220,220,1)",
          pointColor: "rgba(220,220,220,1)",
          pointStrokeColor: "#fff",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(220,220,220,1)",
          data: [25, 25, 25, 25]
        }
      ]
    };

    var myRadarChart = new Chart(ctx).Radar(data,
                                            {responsive: true,
                                             animation: true,
                                             onAnimationComplete: function(){
                                               this.options.animation = true;
                                             }});
    $.ajax({
      url: '/statistics/pgr',
      type: 'post',
      dataType: 'json',
      contentType: false,
      processData: false,
      success: function(result, status, xhr) {
        console.log (result.male);
        console.log(result.female);
        myRadarChart.datasets[0].points[0].value = 3;
        myRadarChart.datasets[0].points[1].value = result.male;
        myRadarChart.datasets[0].points[2].value = 97;
        myRadarChart.datasets[0].points[3].value = result.female;

        myRadarChart.update();
        console.log('ajax post filestorage success: '+ result +' '+ status +' '+ xhr);
      },
      error: function(xhr, status, error) {
        console.log('ajax  post filestorage error: '+ xhr +' '+ status +' '+ error);
      }});

  })();
})();

function updateCharts(result) {
  for (var i=0; i<result.mfcc.length; i++)
    myLineChart.datasets[0].points[i].value = result.mfcc[i];
  myLineChart.update();

  console.log(result.percent);
  moduleDoughnut.segments[1].value = result.predict;
  moduleDoughnut.segments[0].value = 100 - result.predict;
  moduleDoughnut.update();
}
