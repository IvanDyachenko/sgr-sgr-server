(ns sgr-server.views.layout
  (:require [sgr-server.views.language :as lng]
            [hiccup.page :refer [html5 include-css]]))

(defn- main-navigation-bar [& {:keys [language active] :or {language ':en}}]
  (let [str-locale (-> lng/locale language)]
    (html5
     [:div {:class "container"}
      [:div {:class "nav-main"}
       [:div {:class "wrap"}
        [:ul {:class "nav-home nav-notify"}
         [:li [:a {:href (str "/" str-locale)}
               [:img {:class "nav-logo" :src "/img/speakerRecognition.png" :width "38" :height "38"}]
               (-> lng/layout :logo language)]]]
        [:ul {:class "nav-site"}
         (if (= 'recognize active)
           [:li [:a {:href (str "/recognize"  str-locale) :class "active"}
                 (-> lng/layout :recognition language)]]
           [:li [:a {:href (str "/recognize"  str-locale)}
                 (-> lng/layout :recognition language)]])
         (if (= 'train active)
           [:li [:a {:href (str "/train" str-locale) :class "active"}
                 (-> lng/layout :train  language)]]
           [:li [:a {:href (str "/train" str-locale)}
                 (-> lng/layout :train  language)]])
         (if (= 'statistics active)
           [:li [:a {:href (str "/" str-locale) :class "active"};;[:a {:href (str "/statistics" str-locale) :class "active"}
                 (-> lng/layout :statistics  language)]]
           [:li [:a {:href (str "/" str-locale)};;[:a {:href (str "/statistics" str-locale)}
                 (-> lng/layout :statistics  language)]])]]]])))

(defn- end-navigation-bar [& {:keys [language] :or {language ':en}}]
  (html5
   [:div {:class "nav-end" }
    [:div {:class "wrap"}
     [:div {:class "nav-home"} (-> lng/layout :label language)
      [:a {:href (-> lng/layout :chose language) } (-> lng/layout :language language)]]
     [:div {:class "nav-site"} (-> lng/layout :year language)
      [:a {:href "mailto:vandyachen@gmail.com"} (-> lng/layout :author language)]]]]))

(defn common [body & {:keys [language active] :or {language ':en}}]
  (html5
    [:head
     [:title (-> lng/layout :title language)]
     [:link {:rel "shortcut icon" :href "/img/favicon.ico"}]]
   (include-css "/css/styles.css")
   (include-css "/css/navigation.css")
   ;;main navigation bar
   (main-navigation-bar :active active :language language)
   ;;body
   [:body body]
   ;;end navigation bar
   [:footer (end-navigation-bar :language language)]))


