var holder = document.getElementById('my-dropzone'),
    tests = {
      filereader: typeof FileReader != 'undefined',
      dnd: 'draggable' in document.createElement('span'),
      formdata: !!window.FormData,
      progress: "upload" in new XMLHttpRequest
    },
    support = {
      filereader: document.getElementById('filereader'),
      formdata: document.getElementById('formdata'),
      progress: document.getElementById('progress')
    },
    acceptedTypes = {
      'audio/wav': true
    },
    progress = document.getElementById('uploadprogress'),
    fileupload = document.getElementById('upload');

holder.ondragover = function () { return false; };
holder.ondragend  = function () { return false; };
holder.ondrop = function (e) {
  e.preventDefault();
  var file = e.dataTransfer.files[0];
  var fd = new FormData();
  fd.append('record', file);
  $.ajax({
    url: '/recognize/record',
    type: 'post',
    dataType: 'json',
    contentType: false,
    processData: false,
    data: fd,
    success: function(result, status, xhr) {
      updateCharts(result);
      console.log('ajax post filestorage success: '+ result +' '+ status +' '+ xhr);
    },
    error: function(xhr, status, error) {
      console.log('ajax  post filestorage error: '+ xhr +' '+ status +' '+ error);
    }});
};

function postRecord()
{
  recorder.exportWAV(function(blob) { var fd = new FormData();
                                      fd.append('record', blob);
                                      $.ajax({
                                          url: '/recognize/record', //+
                                          type: 'post',                  //+
                                          dataType: 'json',              //+
                                          //enctype: 'multipart/form-data',
                                          contentType: false,
                                          processData: false,
                                          data: fd,
                                          success: function(result, status, xhr) {
                                            updateCharts(result);
                                            console.log('ajax post filestorage success: '+ result +' '+ status +' '+ xhr);
                                           },
                                           error: function(xhr, status, error) {
                                             console.log('ajax  post filestorage error: '+ xhr +' '+ status +' '+ error);
                                           }});});
}

function toggleRecognize(r)
{
  if (!recorder) {
    startAudio(function() {toggleRecording(r); toggleSinewave(r);});
  }
  else {
    toggleSinewave(r);
    if (toggleRecording(r))
    {
      postRecord();
      $(".nav-notify").notify('Click here to save your record.',
                              {arrowShow: false,
                               className: 'info saverecord',
                               showAnimation: 'show',
                               ideAnimation: 'hide',
                               autoHideDelay: 4000});
    }

  }
}


