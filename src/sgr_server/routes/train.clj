(ns sgr-server.routes.train
  (:require [compojure.core       :refer :all]
            [ring.middleware             [multipart-params :as mp]]
            [ring.middleware.json :refer [wrap-json-body wrap-json-response]]
            [ring.util.response   :refer [response]]

            [sgr-server.views.train  :as  view]
            [sgr-server.models.train :as model]))

(defn- train [& {:keys [locale] :or {locale "en_EN"}}]
  (case locale
    "ru_RU" (view/train :language ':ru)
    "en_EN" (view/train :language ':en)
    (view/train)))

(defn- train-record [request]
  (try
    (let [tempfile (:tempfile (:record request))
          gender                (:gender request)
          result   (model/train-record gender tempfile)]
      (println result)
      (response result))
    (catch Exception e
      (do
        (println (str "Exception SGR-server.routes.train-record: " (.getMessage e)))
        {:predict false :mfcc false}))))


(defroutes train-routes
  (GET  "/train"        {{locale :locale} :params} (train :locale locale))
  (POST "/train/record" {params           :params} (train-record params)))
