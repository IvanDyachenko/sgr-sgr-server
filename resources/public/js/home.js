function Colour(col, amt) {
  var usePound = false;
  if (col[0] == "#") {
    col = col.slice(1);
    usePound = true;
  }
  var num = parseInt(col,16);
  var r = (num >> 16) + amt;
  if (r > 255) r = 255;
  else if (r < 0) r = 0;
  var b = ((num >> 8) & 0x00FF) + amt;
  if (b > 255) b = 255;
  else if (b < 0) b = 0;
  var g = (num & 0x0000FF) + amt;
  if (g > 255) g = 255;
  else if (g < 0) g = 0;
  return (usePound?"#":"") + (g | (b << 8) | (r << 16)).toString(16);
}

(function(){

  Chart.defaults.global.responsive = true;


	// MFCC
	(function(){
    var canvas = document.getElementById('mfcc-line'),
        ctx = canvas.getContext('2d');

    var data = {
      labels: ["", "", "", "", "","", "", "", "", "","",""],
      datasets: [
        {
          label: "About recognize",
          fillColor: "rgba(91,144,191,0.4)",
          strokeColor: "rgba(91,144,191,1)",
          pointColor: "rgba(220,220,220,1)",
          pointStrokeColor: "#fff",
          pointHighlightFill: "rgba(91,144,191,0.8)",
          pointHighlightStroke: "rgba(91,144,191,1)",
          data: [52,20,3,7,-3,0,-1,-2,0,0,0,-4,-5]
        }
    ]
    };

    var myLineChart = new Chart(ctx).Line(data,
                                          {responsive: true,
                                           animation: true,
                                           onAnimationComplete: function(){
                                             this.options.animation = true;
                                           }});
  })();

  // Train
	(function(){
    var male   = 67;
    var female = 33;
    var canvas = document.getElementById('train-modular'),
        ctx = canvas.getContext('2d'),
        colours = {"Female": "#4c86b9",
                   "Male": "#b9525c"};
    var moduleData = [
      {
        value: male,
        color: colours["Male"],
        highlight: Colour(colours["Male"], 10),
        label: "percent of Male"
      },
      {
        value: female,
        color: colours["Female"],
        highlight: Colour(colours["Female"], 10),
        label: "percent of Female"
      }];

    var moduleDoughnut = new Chart(ctx).Doughnut(moduleData,
                                          {responsive: true,
                                           animation: true,

                                           onAnimationComplete: function(){
                                             this.options.animation = true;
                                           }});
    $.ajax({
      url: '/statistics/pgr', //+
      type: 'post',                  //+
      dataType: 'json',              //+
      //enctype: 'multipart/form-data',
      contentType: false,
      processData: false,
      success: function(result, status, xhr) {
        moduleDoughnut.segments[0].value = result.male;
        moduleDoughnut.segments[1].value = result.female;
        moduleDoughnut.update();
        console.log('ajax post filestorage success: '+ result +' '+ status +' '+ xhr);
      },
      error: function(xhr, status, error) {
        console.log('ajax  post filestorage error: '+ xhr +' '+ status +' '+ error);
      }});
})();

  //Statistics
  (function(){
    var canvas = document.getElementById('statistics-line'),
        ctx = canvas.getContext('2d');

    var data = {
      labels: ["Error", "Male", "Success", "Female"],
      datasets: [
        {
          label: "My First dataset",
          fillColor:   "rgba(151,187,205,0.2)",
          strokeColor: "rgba(151,187,205,1)",
          pointColor:  "rgba(151,187,205,1)",
          pointStrokeColor:   "#fff",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(151,187,205,1)",
          data: [0, 0, 0, 0]
        },
        {
          label: "My Second dataset",
          fillColor: "rgba(220,220,220,0.2)",
          strokeColor: "rgba(220,220,220,1)",
          pointColor: "rgba(220,220,220,1)",
          pointStrokeColor: "#fff",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(220,220,220,1)",
          data: [25, 25, 25, 25]
        }
      ]
    };

    var myRadarChart = new Chart(ctx).Radar(data,
                                            {responsive: true,
                                             animation: true,
                                             onAnimationComplete: function(){
                                               this.options.animation = true;
                                             }});
     $.ajax({
      url: '/statistics/pgr', //+
      type: 'post',                  //+
      dataType: 'json',              //+
      //enctype: 'multipart/form-data',
      contentType: false,
      processData: false,
      success: function(result, status, xhr) {
        console.log (result.male);
        console.log(result.female);
        myRadarChart.datasets[0].points[0].value = 3;
        myRadarChart.datasets[0].points[1].value = result.male;
        myRadarChart.datasets[0].points[2].value = 97;
        myRadarChart.datasets[0].points[3].value = result.female;

        myRadarChart.update();
        console.log('ajax post filestorage success: '+ result +' '+ status +' '+ xhr);
      },
      error: function(xhr, status, error) {
        console.log('ajax  post filestorage error: '+ xhr +' '+ status +' '+ error);
      }});
  })();


})();
