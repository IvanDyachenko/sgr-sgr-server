(ns sgr-server.views.recognize
  (:require [sgr-server.views.layout   :as layout]
            [sgr-server.views.language :as    lng]
            [hiccup.page :refer [html5 include-css include-js]]))

(defn recognize [& {:keys [language] :or {language ':en}}]
  (layout/common
   (html5
    (include-css "/css/recognize.css")

    [:div {:class "aspect-ratio" :id "my-dropzone"}
           ;;:ondrop "drop(event)" :ondragover "allowDrop(event)"}
     [:canvas {:id "recorder-bar" :width "1920px" :height "1080px"}]
     [:header
      [:input {:id "capture" :onclick "toggleRecognize(\"capture\")"
               :class "micr" :name "capture"
               :type "image" :src "/img/micr.svg" :width "15%"}]]]
    [:div {:class "content-main"}
     [:section {:class "features container"}

      [:article
       [:h3 (-> lng/recognize :mfcc language)]
       [:div {:class "canvas-holder hover-highlight"}
        [:canvas {:id "mfcc-line"
                  :width "400" :height "400"
                  :style "width: 400px; height: 400px; cursor: default;"}]]]

      [:article
       [:h3 (-> lng/recognize :train language)]
       [:div {:class "labeled-chart-container"}
        [:div {:class "canvas-holder hover-highlight"}
         [:canvas {:id "train-modular"
                   :width "400" :height "400"
                   :style "width: 400px; height: 400px; cursor: default;"}]]]]

      [:article
       [:h3 (-> lng/recognize :statistics language)]
       [:div {:class "canvas-holder hover-highlight"}
        [:canvas {:id "statistics-line"
                  :width "400" :height "400"
                  :style "width: 400px; height: 400px; cursor: default;"}]]]

      ]]


    (include-js "/js/jquery.min.js")
    (include-js "/js/notify.js")
    (include-js "/js/recorderjs/recorderWorker.js")
    (include-js "/js/recorderjs/recorder.js")
    (include-js "/js/Chart.min.js")
    (include-js "/js/sinewave.js")
    (include-js "/js/recognize&train.js")
    (include-js "/js/recognize.js"))
   :active 'recognize
   :language language))
