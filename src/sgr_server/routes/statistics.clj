(ns sgr-server.routes.statistics
  (:require [compojure.core           :refer :all]
            [ring.middleware             [multipart-params :as mp]]
            [ring.middleware.json :refer [wrap-json-body wrap-json-response]]
            [ring.util.response   :refer [response]]

            [sgr-server.views.statistics :as view]
            [sgr-server.models.statistics :as model]))

(defn- statistics [& {:keys [locale] :or {locale "en_EN"}}]
  (case locale
    "ru_RU" (view/statistics :language ':ru)
    "en_EN" (view/statistics :language ':en)
    (view/statistics)))

(defn- number-of-<gender>-records [request]
  (try
    (let [result (model/number-of-<gender>-records)]
      (response result))
    (catch Exception e
      (do
        (println (str "Exception SGR-server.routes.number-of-<gender>-records: " (.getMessage e)))
        {:predict false :mfcc false}))))

(defn- percent-of-<gender>-records [request]
  (try
    (let [result (model/percent-of-<gender>-records)]
      (response result))
    (catch Exception e
      (do
        (println (str "Exception SGR-server.routes.percent-of-<gender>-records: " (.getMessage e)))
        {:predict false :mfcc false}))))


(defroutes statistics-routes
  (GET  "/statistics"     {{locale :locale} :params} (statistics :locale locale))
  (POST "/statistics/pgr" {params           :params} (percent-of-<gender>-records  params))
  (POST "/statistics/ngr" {params           :params} (number-of-<gender>-records params)))
