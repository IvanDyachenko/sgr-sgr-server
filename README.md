# SGR-server

Speaker gender recognition via MFCC and SVM [server].

## Prerequisites

You will need [Leiningen][1] 1.7.0 or above installed.

[1]: https://github.com/technomancy/leiningen

## Running

To start a web server for the application, run:

    lein ring server

## License

Copyright © 2014 Dyachenko Ivan

Distributed under the Eclipse Public License either version 1.0 or (at your option) any later version.
