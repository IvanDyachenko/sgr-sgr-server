(ns sgr-server.routes.home
  (:require [compojure.core     :refer :all]
            [sgr-server.views.home :as view]))

(defn- home [& {:keys [locale] :or {locale "en_EN"}}]
  (case locale
    "ru_RU" (view/home :language ':ru)
    "en_EN" (view/home :language ':en)
    (view/home)))

(defroutes home-routes
  (GET "/" {{locale :locale} :params} (home :locale locale)))
