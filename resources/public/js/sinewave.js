(function(window) {

  var Sinewave = function(canvas, analyser, color) {
    var cvs = canvas;
    var ctx = canvas.getContext('2d');
    var anls = analyser;
    var clr = color;
    anls.fftSize = 2048;

    var requestID = null;

    this.clear = function() {
      requestID = null;
    }

    var cvsWidth = cvs.width;
    var cvsHeight = cvs.height;

    var bufferLength = anls.fftSize;
    var dataArray = new Uint8Array(anls.fftSize);

    var devicePixelRatio = window.devicePixelRatio || 1;
    var backingStoreRatio = ctx.webkitBackingStorePixelRatio ||
                             ctx.mozBackingStorePixelRatio ||
                             ctx.msBackingStorePixelRatio ||
                             ctx.oBackingStorePixelRatio ||
                             ctx.backingStorePixelRatio || 1;
    var ratio = devicePixelRatio / backingStoreRatio;
    var oldWidth = cvs.width;
    var oldHeight = cvs.height;

    cvs.width = oldWidth * ratio;
    cvs.height = oldHeight * ratio;

    ctx.fillStyle = '#202020';
    ctx.lineWidth = 3;
    ctx.strokeStyle = clr;


    var update = function (time) {
      cvsWidth = cvs.width;
      cvsHeight = cvs.height;

      ctx.fillRect(0, 0, cvsWidth, cvsHeight);

      ctx.beginPath();

      var sliceWidth = cvsWidth * 1.0 / bufferLength;
      var x = 0;

      anls.getByteTimeDomainData(dataArray);

      for(var i = 0; i < bufferLength; i+=1) {
        var v = dataArray[i] / 128.0;
        var y = v * cvsHeight/2;
        if (i === 0) {
          ctx.moveTo(x, y);
        }
        else {
          ctx.lineTo(x, y);
        }
        x += sliceWidth;
      }

      ctx.lineTo(cvsWidth, cvsHeight/2);
      ctx.stroke();

      requestID = window.requestAnimationFrame(update);
      //console.log(data);

    }

    this.clear = function() {
      cvsWidth = cvs.width;
      cvsHeight = cvs.height;

      ctx.fillRect(0, 0, cvsWidth, cvsHeight);
    }

    this.static = function() {
      cvsWidth = cvs.width;
      cvsHeight = cvs.height;

      ctx.fillRect(0, 0, cvsWidth, cvsHeight);
      ctx.beginPath();
      ctx.moveTo(0, cvsHeight/2);
      ctx.lineTo(cvsWidth, cvsHeight/2);
      ctx.stroke();
      ctx.beginPath();
    }

    this.start = function() {
      requestID = window.requestAnimationFrame(update);
    }

    this.stop = function(){
      cvsWidth = cvs.width;
      cvsHeight = cvs.height;

      ctx.fillRect(0, 0, cvsWidth, cvsHeight);

      window.cancelAnimationFrame(requestID);
      requestID = null;
    }
  };

  window.Sinewave = Sinewave;

})(window);
