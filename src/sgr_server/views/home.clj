(ns sgr-server.views.home
  (:require [sgr-server.views.layout   :as layout]
            [sgr-server.views.language :as    lng]
            [hiccup.page :refer [html5 include-css include-js]]))

(defn home [& {:keys [language] :or {language ':en}}]
  (layout/common
   (html5
    (include-css "/css/home.css")

     [:div {:class "aspect-ratio"}
      [:header
       [:h1 (-> lng/home :h1 language)]
       [:h2 (-> lng/home :h2 language)]
       [:a {:class "btn red"  :href (str "/recognize" (-> lng/locale language))} (-> lng/home :btn-red  language)]
       [:a {:class "btn blue" :href (str "/train"     (-> lng/locale language))} (-> lng/home :btn-blue language)]]]
     [:div {:class "content-main"}
      [:section {:class "features container"}

       [:article
        [:h3 (-> lng/home :mfcc language)]
        [:div {:class "canvas-holder hover-highlight"}
         [:canvas {:id "mfcc-line"
                   :width "400" :height "400"
                   :style "width: 400px; height: 400px; cursor: default;"}]]
        [:p (-> lng/home :about-recognize language)]]

        [:article
        [:h3 (-> lng/home :train language)]
        [:div {:class "labeled-chart-container"}
         [:div {:class "canvas-holder hover-highlight"}
          [:canvas {:id "train-modular"
                    :width "400" :height "400"
                    :style "width: 400px; height: 400px; cursor: default;"}]]]
        [:p (-> lng/home :about-train language)]]

       [:article
        [:h3 (-> lng/home :statistics language)]
        [:div {:class "canvas-holder hover-highlight"}
         [:canvas {:id "statistics-line"
                   :width "400" :height "400"
                   :style "width: 400px; height: 400px; cursor: default;"}]]
        [:p (-> lng/home :about-statistics language)]]

       ]]
    (include-js "/js/jquery.min.js")
    (include-js "/js/Chart.min.js")
    (include-js "/js/home.js"))
    :language language))
