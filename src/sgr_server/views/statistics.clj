(ns sgr-server.views.statistics
  (:require [sgr-server.views.language :as lng]
            [sgr-server.views.layout :as layout]
            [hiccup.page :refer [html5 include-css include-js]]))

(defn statistics [& {:keys [language] :or {language ':en}}]
 (layout/common
   (html5
    (include-css "/css/statistics.css")


    [:section {:class "features container"}

     [:section {:class "docs-container"}
      [:article {:class "intro"}
       [:h1 (-> lng/train :h1 language)]]
      ;; [:h3 (-> lng/train :intro language)]]
      [:article {:id "recording"}
       [:h2 (-> lng/train :record-article language)]
       ;;[:h3 "Include Chart.js"]
       [:p (-> lng/train :record-text-first language)
        [:img {:type "image" :src "/img/micr.svg" :width "5%"}]
        (-> lng/train :record-text-second language)]]

      [:article {:id "gender"}
       [:h2 (-> lng/train :gender-article language)]
       ;;[:h3 "Include Chart.js"]
       [:p (-> lng/train :gender-text language)]]

      [:article {:id "upload"}
       [:h2 (-> lng/train :upload-article language)]
       ;;[:h3 "Include Chart.js"]
       [:p (-> lng/train :upload-text language)]]
      ]]
    ;;[:div {:class "aspect-ratio"}
    ;; [:header ]]
    ;; [:div {:class "content-main"}
    ;;  [:section {:class "features container"}]]

    ;;(include-js "/js/jquery.min.js")
    ;;(include-js "/js/notify.min.js")
    ;;(include-js "/js/Chart.min.js"))
    )
   :active 'statistics
   :language language))
