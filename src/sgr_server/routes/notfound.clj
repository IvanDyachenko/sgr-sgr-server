(ns sgr-server.routes.notfound
  (:require [compojure.core       :refer :all]
            [sgr-server.views.layout :as view]))

(defn notfound []
  (view/common [:h1 "Not found"]))
