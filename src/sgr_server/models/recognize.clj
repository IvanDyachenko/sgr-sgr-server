(ns sgr-server.models.recognize
  (:require [sgr-server.config        :as     cfg]
            [sgr-server.models.common :refer :all]

            [sgr-sound.converter :refer [sound->convert->mfcc->average]]

            [sgr-svm.predict  :as svm]
            [sgr-svm.dataset  :refer [mfcc->svm-mfcc]]))

(defn recognize-record
  ([tempfile]
   (try
     (let [source-target (get-source-target-path tempfile)
           sourceFile (clojure.java.io/file (:sourcePath source-target))
           targetFile (clojure.java.io/file (:targetPath source-target))]
       (recognize-record sourceFile targetFile))
     (catch Exception e
       (do
         (println (str "Exception SGR-server.models.recognize-record: " (.getMessage e)))
         {:predict false :mfcc false}))))
  ([sourceFile targetFile]
   (try
     (let [convert (sound->convert->mfcc->average sourceFile targetFile)]
       (if convert
         (let [mfcc    (:mfcc convert)
               predict (svm/predict ((:model cfg/svm-config)) (mfcc->svm-mfcc mfcc))]
           {:predict (int (percent-predict predict)) :mfcc (map #(int %) mfcc)})
         {:predict false :mfcc false}))
     (catch Exception e
       (do
        (println (str "Exception SGR-server.models.recognize-record: " (.getMessage e)))
         {:predict false :mfcc false})))))

