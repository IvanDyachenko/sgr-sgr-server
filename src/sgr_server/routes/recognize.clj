(ns sgr-server.routes.recognize
  (:require [compojure.core       :refer :all]
            [ring.middleware             [multipart-params :as mp]]
            [ring.middleware.json :refer [wrap-json-body wrap-json-response]]
            [ring.util.response   :refer [response]]

            [sgr-server.views.recognize  :as  view]
            [sgr-server.models.recognize :as model]))

(defn- recognize [& {:keys [locale] :or {locale "en_EN"}}]
  (case locale
    "ru_RU" (view/recognize :language ':ru)
    "en_EN" (view/recognize :language ':en)
    (view/recognize)))

(defn- recognize-record [request]
  (try
    (let [tempfile (:tempfile (:record request))
          result   (model/recognize-record tempfile)]
      (println result)
      (response result))
    (catch Exception e
      (do
        (println (str "Exception SGR-server.routes.recognize-record: " (.getMessage e)))
        {:predict false :mfcc false}))))

(defroutes recognize-routes
  (GET  "/recognize"        {{locale :locale} :params} (recognize :locale locale))
  (POST "/recognize/record" {params           :params} (recognize-record  params)))
