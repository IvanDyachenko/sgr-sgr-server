(ns sgr-server.handler
  (:require [compojure.handler :as handler]
            [compojure.route   :as route]
            [compojure.core            :refer [defroutes routes]]
            [ring.middleware.resource  :refer [wrap-resource]]
            [ring.middleware.file-info :refer [wrap-file-info]]
            [ring.middleware.json      :as     middleware]

            [sgr-server.routes.notfound   :refer [notfound]]
            [sgr-server.routes.home       :refer [home-routes]]
            [sgr-server.routes.recognize  :refer [recognize-routes]]
            [sgr-server.routes.statistics :refer [statistics-routes]]
            [sgr-server.routes.train      :refer [train-routes]]))


(defn init []
  (println "SGR [server] is starting"))

(defn destroy []
  (println "SGR [server] is shutting down"))

(defroutes app-routes
  (route/resources "/")
  (route/not-found "Not Found"))

(def app
  (-> (routes home-routes train-routes recognize-routes statistics-routes app-routes)
      (handler/site)
      (middleware/wrap-json-body {:keywords? true :bigdecimals? true})
      (middleware/wrap-json-response)))
