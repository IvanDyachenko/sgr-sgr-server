(defproject sgr/sgr-server "0.0.1"
  :description "Speaker gender recognition via MFCC and SVM [server]."
  :url "https://bitbucket.org/IvanDyachenko/sgr-sgr-server"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [compojure      "1.1.6"]
                 [hiccup         "1.0.5"]
                 [ring-server    "0.3.1"]
                 [ring/ring-json "0.3.1"]

                 [sgr/sgr-db     "0.0.2"]
                 [sgr/sgr-sound  "0.0.3"]
	               [sgr/sgr-svm    "0.0.6"]] ;;[0.0.4]
  :plugins [[lein-ring "0.8.12"]]
  :ring {:handler sgr-server.handler/app
         :init sgr-server.handler/init
         :destroy sgr-server.handler/destroy}
  :profiles
  {:uberjar {:aot :all}
   :production
   {:ring
    {:open-browser? false, :stacktraces? false, :auto-reload? false}}
   :dev
   {:dependencies [[ring-mock "0.1.5"] [ring/ring-devel "1.3.1"]]}})
